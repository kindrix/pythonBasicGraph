from node import Node
from vector import Vector

#DrawableNode is a subclass of Node
class DrawableNode(Node):
    def __init__(self, name, rad=10, loc=Vector(0,0), fillColor = "yellow"):
        Node.__init__(self, loc)
        self.name = name
        self.rad = rad
        self.fillColor = fillColor
        self.tag = "node"+name


    #return a partial copy of a DrawableNode
    @staticmethod
    def getSimpleCopy(u):
        v = DrawableNode(u.name, u.rad, Vector(u.loc.x, u.loc.y))
        return v


    #draws a node on the canvas and adds tag to identify node: lbl_node.name
    @staticmethod
    def drawNode(u, canvas):
        u.oval = canvas.create_oval(u.loc.x - u.rad, u.loc.y - u.rad, u.loc.x + u.rad, u.loc.y + u.rad,
        fill = u.fillColor, tag = u.tag)
        canvas.create_text(u.loc.x, u.loc.y, text=u.name, tag="lbl_"+u.name)


    #draws an edge betweentwo nodes u and v, and adds tag to identify line:line_u.name_v.name
    @staticmethod
    def drawEdge(u, v, canvas):
        dirVectors = DrawableNode.getEdgeVector(u,v)
        dirUV = dirVectors[0]
        dirVU = dirVectors[1]
        #draw line  after adding adjustment vector and tag line
        canvas.create_line(u.loc.x + dirUV.x , u.loc.y + dirUV.y ,
        v.loc.x + dirVU.x, v.loc.y + dirVU.y, tag="line_" +u.name +"_"+v.name)

    #get direction and adjustment vector so lines are drawn from edge of circle
    @staticmethod
    def getEdgeVector(u, v):

        dirUV = Vector.getDirVector(u.loc, v.loc)
        dirUV.normalize()
        dirUV.mult(u.rad)
        dirVU = Vector.getCopy(dirUV)
        dirVU.mult(-1)
        return (dirUV, dirVU)

    #move node by given dx and dy, and redraw edges to neighbours
    @staticmethod
    def moveNode(u, neighbours, canvas, deltaX, deltaY):

        #update location on canvas
        canvas.move(u.tag, deltaX, deltaY)

        #update location vector inside node
        u.applyForce(Vector(deltaX, deltaY))
        u.updateLocation()

        #update location of node label
        canvas.move("lbl_"+u.name, deltaX, deltaY)

        #update edges originating from node
        DrawableNode.redrawEdges(u, neighbours, canvas)

    @staticmethod
    def redrawEdges(u, neighbours, canvas):
        for v in neighbours:
            #get line via their tags
            tagUV = "line_" + u.name+ "_" + v.name
            tagVU = "line_" + v.name+ "_" + u.name

            #get coords of current line
            lineCoords = canvas.coords(tagUV)

            #find adjustment and direction vector u-v and v-u
            dirVectors = DrawableNode.getEdgeVector(u,v)
            dirUV = dirVectors[0]
            dirVU = dirVectors[1]

            #redraw line u to v
            canvas.coords(tagUV, u.loc.x + dirUV.x , u.loc.y + dirUV.y  ,
            v.loc.x+dirVU.x, v.loc.y+dirVU.y)

            #redraw line v to u
            canvas.coords(tagVU, v.loc.x+dirVU.x, v.loc.y+dirVU.y,
            u.loc.x+dirUV.x,  u.loc.y+dirUV.y)

    #experimental method, scale if position of nodes beyond window
    @staticmethod
    def scaleToCanvas(nodeList, WIDTH, HEIGHT):
        z = [u.loc for u in nodeList]
        #print z
        xs = [loc.x for loc in z]
        minX = 0 if min(xs)>=0 else min(xs)
        maxX = max(xs)
        ys = [loc.y for loc in z]
        minY = 0 if min(ys)>=0 else min(ys)
        maxY = max(ys)
        #print xs
        #print ys

        #print "minX:",minX, " maxX:", maxX, " minY:", minY, " maxY:", maxY
        if (minX<0 or minY<0):
            for u in nodeList:
                #print u.name + " node loc prior:" + str(u.loc)
                #u.applyForce(Vector(abs(minX)+2*u.rad , abs(minY)+2*u.rad))

                if (u.loc.x == minX and u.loc.y == minY):
                    u.applyForce(Vector(abs(minX)+2*u.rad , abs(minY)+2*u.rad))
                elif (u.loc.x == minX):
                    u.applyForce(Vector(abs(minX)+2*u.rad , abs(minY)))
                elif (u.loc.y == minY):
                    u.applyForce(Vector(abs(minX) , abs(minY)+2*u.rad))
                else:
                    u.applyForce(Vector(abs(minX) , abs(minY)))
                u.updateLocation()

                #print u.name + " node loc post:" + str(u.loc)
        scale = max([u.loc.mag() for u in nodeList])

        for u in nodeList:
            u.loc.mult(WIDTH/scale)
            # u.applyForce(Vector(10,10))
            # u.updateLocation()
        # print "-------- after scaling -------------"
        # z= [u.loc for u in nodeList]
        # print z
