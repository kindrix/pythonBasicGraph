
from vector import Vector
import math

#A node has a location, velocity, and acceleration
class Node:
    def __init__(self, loc = Vector(10,10), velocity=Vector(0,0), acceleration = Vector(0,0)):
        self.loc = loc
        self.velocity = velocity
        self.acceleration = acceleration

    #updates the location of node by first updating the velocity and then adding the velocity vector to the location
    def updateLocation(self):
        self.velocity.add(self.acceleration)
        self.loc.add(self.velocity)

        #Since we are dealing with static nodes (no velocity, acceleration) we reset acc and velocity to 0
        self.acceleration.mult(0)
        self.velocity.mult(0)

    #applies given force to node's acceleration
    #result of newtwon's second law, f = ma, m is taken to be 1
    def applyForce(self, force):
        self.acceleration.add(force)
