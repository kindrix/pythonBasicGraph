#!/usr/bin/python
import random
from drawableNode import DrawableNode
from vector import Vector
from Tkinter import *


#window and node
WIDTH = 600
HEIGHT = 600
RAD = 15

#set up drawing window and canvas
tk = Tk()
canvas = Canvas(tk, width  = WIDTH, height = HEIGHT)
tk.title("Drawing")
canvas.pack()

#create nodes
#param: node name, radius, location vector(x,y)
a = DrawableNode("a", RAD , Vector(random.randrange(RAD, WIDTH),random.randrange(RAD, HEIGHT)))
b = DrawableNode("b", RAD , Vector(random.randrange(RAD, WIDTH),random.randrange(RAD, HEIGHT)))
c = DrawableNode("c", RAD , Vector(random.randrange(RAD, WIDTH),random.randrange(RAD, HEIGHT)))
d = DrawableNode("d", RAD , Vector(random.randrange(RAD, WIDTH),random.randrange(RAD, HEIGHT)))
e = DrawableNode("e", RAD , Vector(random.randrange(RAD, WIDTH),random.randrange(RAD, HEIGHT)))
f = DrawableNode("f", RAD , Vector(random.randrange(RAD, WIDTH),random.randrange(RAD, HEIGHT)))


#create dictionary to access nodes easily
nodeDict = {"a":a,"b":b,"c":c,"d":d,"e":e,"f":f}

#create graph
graph = { "a": [c],
          "b" : [c, e],
          "c" : [a, b, d, e],
          "d" : [c],
          "e" : [c, b],
          "f" : []
          }

# a = DrawableNode("1", RAD , Vector(100,100))
# b = DrawableNode("2", RAD ,  Vector(600,600))
# c = DrawableNode("3", RAD ,  Vector(400,400))
#
# nodeDict ={"1":a, "2":b, "3":c}
# graph={"1":[b,c], "2":[a], "3":[a]}

#draw initial nodes
for u in nodeDict.values():
    DrawableNode.drawNode(u, canvas)

#draw edges
for nodeName in nodeDict:
    for node in graph[nodeName]:
        DrawableNode.drawEdge(nodeDict[nodeName], node, canvas)

#event method for moving node
def callback(event, u):
    #print "clicked at", event.x, event.y
    deltaX = event.x - u.loc.x
    deltaY = event.y - u.loc.y
    DrawableNode.moveNode(u, graph[u.name], canvas, deltaX, deltaY)

#bind node drag events
for u in nodeDict.values():
    canvas.tag_bind(u.oval, '<B1-Motion>', lambda event, obj=u: callback(event, obj))

tk.mainloop()
