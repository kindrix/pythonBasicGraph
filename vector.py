import math

class Vector:
    def __init__(self, x, y):
        self.x  = float(x)
        self.y = float(y)

    def add(self, v):
        self.x = self.x + v.x
        self.y = self.y + v.y

    def sub(self, v):
        self.x = self.x - v.x
        self.y = self.y - v.y

    def mult(self, s):
        self.x = self.x * s
        self.y = self.y * s

    def div(self, s):
        self.x = self.x / s
        self.y = self.y / s

    def mag(self):
        return math.sqrt(math.pow(self.x, 2) + math.pow(self.y, 2))

    def normalize(self):
        m = self.mag()
        if (m != 0):
            self.div(m)

    def getCopy(self):
        a = Vector(self.x, self.y)
        return a

    def __str__(self):
        return "("+ str(self.x) + ", " + str(self.y) + ")"

    def __repr__(self):
        return "("+ str(self.x) + ", " + str(self.y) + ")"


    @staticmethod
    def add1(v, w):
        return Vector(v.x + w.x, v.y + w.y)

    #subtract vector w from v
    @staticmethod
    def sub1 (v, w):
        return Vector(v.x - w.x, v.y - w.y)

    #vector from v to w
    @staticmethod
    def getDirVector(v, w):
        res = Vector.sub1(v,w)
        res.mult(-1)
        return res
