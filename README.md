# Simple Graph Python
*Basic graph drawing in Python using Python*

There is a demo file, *main.py*, to see the implementation in action. You can drag around the  nodes by holding down the mouse and moving it around. The edges will readjust automatically.

![alt text](https://gitlab.com/kindrix/pythonBasicGraph/raw/f3f7385a82c6d592a4528f18842fe728c6f07180/simpleGraph.png "Simple Graph")

**Note**: Noticed some latency in detecting mouse event on my Mac.

## Files
1. vector.py

    Contains Vector class consisting of two points and basic vector operations.
2. node.py

    Contains Node class consisting of a location, velocity and acceleration vectors.
3. drawableNode.py

    Contains DrawableNode class, a subclass of the Node class but also contains attributes to help with drawing it on the screen.
4. main.py

    Demo file. Execute using *./main.py* to see display graph
